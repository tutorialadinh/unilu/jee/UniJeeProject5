# Java Enterprise Web App - Project 5 for the university of Luxembourg


### 1. Commands to use for deploying the Spring Boot App

###### Build the package
```Maven
mvn clean package
```
###### Start Spring Boot
```Maven
mvn spring-boot:start
```

###### Stop Spring Boot
>
> If you start your app via mvn spring-boot:start, then fork mode is enabled by default. However, when you run mvn spring-boot:stop, fork mode is no longer defaulted to true (it's null), so you need to specify it, i.e. add **-Dspring-boot.stop.fork** to your command.
>

```Maven
mvn spring-boot:stop -Dspring-boot.stop.fork
```

<br/>

### 2. Interactions between Java classes
Requests are caught by `StatecController.java`.

In the controller, the argument will go through a series of checking points before calling `JsonServiceImpl.java` through its interface `JsonService.java`.   

Then, this is inside `StatecDto.java`, we are treating data : we defined  the algorithms there and we interact with the entities like `StatecEntity.java` and `StatecEntityCategory.java`

Once an exception is caught, `ControllerExceptionHandler.java` will send a JSON error message.

<!--- Center image --->
<p align="center">
  <a href="doc/Classes_Interaction.jpg" target="_blank">
    <img src="doc/Classes_Interaction.jpg" alt="schema" style="height: 250px;width:500px;"/>
  </a>
</p>

<br/>

### 3. How to use Web App

Go to `http://localhost:8080/` : 
>
> You will be redirected to the API page. 
>
> http://localhost:8080/exercise5/api
>
> Configurations can be found in `application.properties` file.
>

<br/>

I've created a custom 404 Error page in `resources/templates/error` where you can click on the Button to be redirected to http://localhost:8080/exercise5/api/swagger-ui/index.html?configUrl=/exercise5/api/swagger-config.

<br/>

You also need to disable the Whitelabel page in `application.properties`

```text
server.error.whitelabel.enabled=false
```

<!--- Center image --->
<p align="center">
  <a href="doc/Custom_404_Error_Page.jpg" target="_blank">
    <img src="doc/Custom_404_Error_Page.jpg" alt="error" style="height: 250px;width:250px;"/>
  </a>
  <a href="doc/Swagger_UI.jpg" target="_blank">
    <img src="doc/Swagger_UI.jpg" alt="swagger" style="height: 250px;width:250px;"/>
  </a>
</p>

<br/>

Then, you can test 2 GET endpoints. 

~~To distinguish them, I set a RegexPattern in the `@GetMapping` of the 1st one to accept all except the `-` character (`{month:[^-]*}`).~~

* ~~/exercise5/api/statec/{month:[^-]*}~~
* ~~/exercise5/api/statec/{start}-{end}~~

<br/>

>
> <ins>**UPDATE 1**</ins>
>
> According to https://stackoverflow.com/questions/65666513/how-to-differentiate-two-versions-of-an-api-method-in-swagger-documentation
>
> You have to specify different URL to distinguish both of them when **<ins>using Swagger UI</ins>**.
>
> * /exercise5/api/statec/**v1**/{month}
> * /exercise5/api/statec/**v2**/{start}-{end}
>
> Like this, you don't have to define Regex in `@GetMapping`.
>

<br/>

>
> <ins>**UPDATE 2**</ins>
>
> * ~~/exercise5/api/statec/**v1**/{month}~~
> * ~~/exercise5/api/statec/**v2**/{start}-{end}~~
>
> * /exercise5/api/statec/**getMonth**/{month}
> * /exercise5/api/statec/**getRange**/{start}-{end}
>

<br/>

##### The exceptions
I've created a class named `ControllerExceptionHandler.java` containing an `@RestControllerAdvice` to return a JSON message when an exception occurs (called with another annotation `@ResponseStatus`).

<br/>


There are 3 exceptions I've defined. Each time an exception occurred, a `throw` method is called :

>
> FormatException (for wrong input)
>
> NoDataFoundException (if we return an empty result)
>
> ServerException (if for example we deleted by inattention the statec.xml file )
>

<br/>

##### The input conditions
After some attempts, I've decided to declare the Rest endpoint arguments as `String` (instead of `jav.util.Date`). 

<br/>

Here are the conditions to pass for getting the results:

* If String **is empty or contains ONLY white spaces**
* If String **, while containing numbers, has some white spaces**
* If String input (specificMonth, start or end) has a **length of 6 characters** (because of the `yyyyMM` format)
* If String input does **not contain any characters** (we will use `Long.parseLong()` if we enter <ins>GREAT</ins> numbers)
* When parsing String to Long, check **if value is positive**
* If the 2 last characters of String input represents the **month Number** (included in the `01` - `12` range)
* **If `start` Date really occurs before `end` Date** (This is also a good opportunity to check if the 2 `String` values can be converted to `Date`)

<br/>

All the checking occurs in the `StatecController.java`.