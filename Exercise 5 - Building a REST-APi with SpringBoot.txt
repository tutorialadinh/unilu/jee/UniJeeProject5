Exercise 5: Building a REST-APi with SpringBoot

In this exercise, you are requested to use SpringBoot to build a REST API with JSON encoding accessible under the URL http://localhost:8080/exercise5 . 
This REST API should use the data set provided in Exercise 4 (the Statec Employment statistics) - you can reuse the provided XML file. 

You should provide the following functionalities:

    get all the data for a specific month (check error conditions !!) - the month format should be "YYYYMM" (202106 for example)
    get all the data for a specific month range: example "202101-202106"
    determine (for each category) the growth/reduction for a specific month or month range

As a small challenge, you should also provide a documentation of your API using OpenAPI library (formerly known Swagger). 
I leave it to you to find out how you can integrate OpenAPI into SpringBoot and set up this documentation. 

The documentation should be accessible at http://localhost:8080/exercise5/api .


Upload the complete code and an optional textual README with some additional information (especially: Gradle or maven used ?).