package lu.uni.UniJeeProject5.controller;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.links.Link;
import io.swagger.v3.oas.annotations.links.LinkParameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lu.uni.UniJeeProject5.exception.message.FormatException;
import lu.uni.UniJeeProject5.model.StatecEntity;
import lu.uni.UniJeeProject5.service.JsonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping(path = "/")
public class StatecController {
    private static final String YEAR_MONTH_PATTERN = "yyyyMM";
    private static final String WHITESPACE_PATTERN = "\\s";
    private static final int NUMBER_DIGITS = 6;
    private static final int MAX_MONTH_NUMBER = 12;

    @Autowired
    @Qualifier("JsonParser")
    JsonService service;


    @Hidden
    @GetMapping("/")
    public ResponseEntity<Object> redirectApiDocumentation() {
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(URI.create("/exercise5/api"));
        return new ResponseEntity<>(headers, HttpStatus.MOVED_PERMANENTLY);
    }

    /**
     * Method to get a list of StatecEntity based on one specific Month.
     * <p>
     * To distinguish with th other endpoint 'displaySpecificPeriod()', we added a regex in @GetMapping to accept all characters except '-'
     */
    @Operation(summary = "Get a list of STATEC entities based on a specific month in yyyyMM format", operationId = "displaySpecificMonth", responses = {
            @ApiResponse(responseCode = "200", description = "Results found", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = StatecEntity.class))}, links = {
                    @Link(name = "Get STATEC data for a specific month", operationId = "displaySpecificMonth", parameters = {
                            @LinkParameter(name = "month", expression = "202004")
                    })
            }),
            @ApiResponse(responseCode = "400", description = "Invalid input (empty, wrong format)", content = @Content)
    })
    @GetMapping(path = "/exercise5/api/statec/getMonth/{month}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> displaySpecificMonth(@Parameter(description = "Allowed format: yyyyMM") @PathVariable String month) {
        List<String> errorLogs = new ArrayList<>();

        // Test if a month has 6 digits (@Pattern only works with input fields in Swagger UI, not with URL)
        try {
            // Check if specific Month contains some white spaces with numbers
            Pattern pattern = Pattern.compile(WHITESPACE_PATTERN);
            Matcher matcher = pattern.matcher(month);

            if (matcher.find())
                errorLogs.add("Specific Month contains some white spaces");


            // Check if specific Month has 6 digits (we're using Long if we input a number too much greater), is positive and has no characters
            if (Long.parseLong(month) < 0)
                errorLogs.add("Specific month must be positive");


            if (month.length() != NUMBER_DIGITS)
                errorLogs.add("Specific month does not exactly have 6 characters. Allowed format yyyyMM.");


            // Check if month number is not greater than 12 or equals to 0 (should be included in 01-12 range)
            checkMonthNumber(month, errorLogs);

        } catch (NumberFormatException e) {
            errorLogs.add("Specific month has characters. Only numbers are allowed.");
        }

        // If everything is OK
        if (errorLogs.size() == 0) {
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("status", HttpStatus.OK.value());
            body.put("timestamp", LocalDate.now());
            body.put("data", service.displaySpecificMonth(month));

            return new ResponseEntity<>(body, HttpStatus.OK);
        } else {
            throw new FormatException(errorLogs);
        }
    }


    /**
     * Method to get a list of StatecEntity based on a month range.
     */
    @Operation(summary = "et a list of STATEC entities based on a specific month range in yyyyMM-yyyyMM format", operationId = "displaySpecificPeriod", responses = {
            @ApiResponse(responseCode = "200", description = "Results found", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = StatecEntity.class))}, links = {
                    @Link(name = "Get STATEC data for a specific month range", operationId = "displaySpecificPeriod", parameters = {
                            @LinkParameter(name = "start", expression = "202004"),
                            @LinkParameter(name = "end", expression = "202008")
                    })
            }),
            @ApiResponse(responseCode = "400", description = "Invalid input (empty, wrong format)", content = @Content)
    })
    @GetMapping(path = "/exercise5/api/statec/getRange/{start}-{end}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> displaySpecificPeriod(@Parameter(description = "Allowed format: yyyyMM") @PathVariable String start, @Parameter(description = "Allowed format: yyyyMM") @PathVariable String end) {
        List<String> errorLogs = new ArrayList<>();

        // Test if a period has no characters, has 6 digits, etc.
        try {
            // Check if start, end Date are empty or ONLY contain white spaces
            if (start.isBlank())
                errorLogs.add("start Date is empty or only contains white spaces");

            if (end.isBlank())
                errorLogs.add("end Date is empty or only contains white spaces");


            // Check if both of them have some white spaces with numbers
            Pattern pattern = Pattern.compile(WHITESPACE_PATTERN);
            Matcher matcherStart = pattern.matcher(start);
            Matcher matcherEnd = pattern.matcher(end);

            if (matcherStart.find())
                errorLogs.add("start Date contains some white spaces");

            if (matcherEnd.find())
                errorLogs.add("end Date contains some white spaces");


            // Check if start, end Date have 6 digits (we're using Long if we enter a number too much greater), are positive and have no characters
            if (Long.parseLong(start) < 0)
                errorLogs.add("start Date must be positive");

            if (start.length() != NUMBER_DIGITS)
                errorLogs.add("start Date does not exactly have 6 characters. Allowed format yyyyMM.");

            if (Long.parseLong(end) < 0)
                errorLogs.add("end Date must be positive");

            if (end.length() != NUMBER_DIGITS)
                errorLogs.add("end Date does not exactly have 6 characters. Allowed format yyyyMM.");


            // Check if month number is not greater than 12 or equals to 0 (should be included in 01-12 range)
            checkMonthNumber(start, errorLogs);
            checkMonthNumber(end, errorLogs);

            // Check if start Date really occurs before end Date
            checkPeriodOrder(start, end, errorLogs);

        } catch (NumberFormatException e) {
            errorLogs.add("One of the Date (start or end) values has characters or is empty. In this 'yyyyMM-yyyyMM' format, only numbers are allowed.");
        }

        // If everything is OK
        if (errorLogs.size() == 0) {
            Map<String, Object> body = new LinkedHashMap<>();
            body.put("status", HttpStatus.OK.value());
            body.put("timestamp", LocalDate.now());
            body.put("data", service.displaySpecificPeriod(start, end));

            return new ResponseEntity<>(body, HttpStatus.OK);
        } else {
            throw new FormatException(errorLogs);
        }
    }


    // =========================================================================================================================================================

    private void checkMonthNumber(String monthString, List<String> errorLogs) {
        // If monthString = 202046, begin from 46
        String monthNumberForCheck = monthString.substring(4);

        // monthNumber should be between 01 and 12.
        if (Integer.parseInt(monthNumberForCheck) > MAX_MONTH_NUMBER || Integer.parseInt(monthNumberForCheck) == 0)
            errorLogs.add("Format exception for " + monthString + ". Returns a month number greater than 12 or equals to 0 : " + monthNumberForCheck);
    }

    private void checkPeriodOrder(String start, String end, List<String> errorLogs) {
        DateFormat formatter = new SimpleDateFormat(YEAR_MONTH_PATTERN);

        try {
            // If start Date occurs after end Date
            if (formatter.parse(start).compareTo(formatter.parse(end)) > 0)
                errorLogs.add("start Date " + start + " occurs after end Date " + end + ". Change the order.");

        } catch (ParseException e) {
            errorLogs.add("Format exception for " + start + " or " + end + " when determining if start Date really occurs before end Date");
        }
    }
}
