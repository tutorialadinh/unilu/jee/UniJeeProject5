package lu.uni.UniJeeProject5.dto;

import lu.uni.UniJeeProject5.exception.message.NoDataFoundException;
import lu.uni.UniJeeProject5.exception.message.ServerException;
import lu.uni.UniJeeProject5.model.StatecEntity;
import lu.uni.UniJeeProject5.model.StatecEntityCategory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component("JsonDto")
public class StatecDto {
    private static final String MONTH_NAME_YEAR_PATTERN = "MMMM yyyy";
    private static final String YEAR_MONTH_PATTERN = "yyyyMM";


    public List<StatecEntity> getSpecificMonthFromXml(String month) {
        // Initialize
        List<StatecEntity> listOfEntity = new ArrayList<>();
        StatecEntity statecEntity = new StatecEntity();
        StatecEntity tempEntity = new StatecEntity();
        boolean isLastDataFromXml = false;
        boolean dateFound = false;
        boolean needNextXmlData = false;


        Document doc;
        try {
            doc = parseXML(Thread.currentThread().getContextClassLoader().getResourceAsStream("statec.xml"));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.out.println("STATEC XML file not found or can't be read : " + e);
            throw new ServerException("There is a server error. The STATEC XML file can't be found or there is a XML syntax error. Please call the Center.");
        }

        // Get the list of "Row" where they stored the Date and the value
        NodeList rowNodeList = doc.getElementsByTagName("Row");

        for (int k = 0; k < rowNodeList.getLength(); k++) {
            // If(k ==0 ), we are treating the first Data XML which is the most current Date (not the oldest)
            if (k == rowNodeList.getLength() - 1)
                isLastDataFromXml = true;

            // Check its children ("RowLabels" and "Cells")
            Node nNode = rowNodeList.item(k);

            for (int j = 0; j < nNode.getChildNodes().getLength(); j++) {
                // If getNodeName() = "#text" (indent), we ignore
                if (!nNode.getChildNodes().item(j).getNodeName().equals("#text")) {

                    if (nNode.getChildNodes().item(j).getNodeName().equals("RowLabels")) {
                        // Date
                        Element eElement = (Element) nNode;
                        Element contentElement = (Element) eElement.getElementsByTagName("RowLabel").item(0);

                        SimpleDateFormat formatterYearMonth = new SimpleDateFormat(YEAR_MONTH_PATTERN);
                        SimpleDateFormat formatterMonthName = new SimpleDateFormat(MONTH_NAME_YEAR_PATTERN);

                        try {
                            Date monthDate = formatterYearMonth.parse(month);

                            if (contentElement.getTextContent().equals(formatterMonthName.format(monthDate))) {
                                statecEntity.setDateValue(contentElement.getTextContent());
                                dateFound = true;
                            }
                        } catch (ParseException e) {
                            System.out.println("Issue when parsing month String to Date or when converting it into MMMM yyyy format : " + e);
                            throw new ServerException("There is an issue when parsing month String to Date or when converting it into MMMM yyyy format. Please call the Center.");
                        }

                        // Once we found our Data and isFirstDataFromXml = false, we need to read the next XML data (= the previous month)
                        if (needNextXmlData) {
                            /*
                             * As we return Data for ONLY one month AND as the list of Date in our XML is in Descending order,
                             * when we found Data for, example July 2020, we need to read the next value in XML to get data about its previous month, June 2020.
                             */
                            tempEntity.setDateValue(contentElement.getTextContent());
                        }
                    }

                    if (dateFound) {
                        // Then get the other values
                        if (nNode.getChildNodes().item(j).getNodeName().equals("Cells")) {
                            // Cell List
                            Element eElement2 = (Element) nNode;
                            NodeList cellNodeList = eElement2.getElementsByTagName("C");

                            // Once we found our Data and isFirstDataFromXml = false, we need to read the next XML data (= the previous month)
                            if (needNextXmlData) {
                                /*
                                 * As we return Data for ONLY one month AND as the list of Date in our XML is in Descending order,
                                 * when we found Data for, example July 2020, we need to read the next value in XML to get data about its previous month, June 2020.
                                 */

                                // Storing C value into entity for next XML data (previous month which will not be displayed in our JSON response)
                                tempEntity.setResidentBorderers(new StatecEntityCategory(cellNodeList.item(0).getAttributes().getNamedItem("f").getNodeValue()));
                                tempEntity.setNotResidentBorderers(new StatecEntityCategory(cellNodeList.item(1).getAttributes().getNamedItem("f").getNodeValue()));
                                tempEntity.setNationalWageEarners(new StatecEntityCategory(cellNodeList.item(2).getAttributes().getNamedItem("f").getNodeValue()));
                                tempEntity.setDomesticWageEarners(new StatecEntityCategory(cellNodeList.item(3).getAttributes().getNamedItem("f").getNodeValue()));
                                tempEntity.setNationalSelfEmployment(new StatecEntityCategory(cellNodeList.item(4).getAttributes().getNamedItem("f").getNodeValue()));
                                tempEntity.setDomesticSelfEmployment(new StatecEntityCategory(cellNodeList.item(5).getAttributes().getNamedItem("f").getNodeValue()));
                                tempEntity.setNationalEmployment(new StatecEntityCategory(cellNodeList.item(6).getAttributes().getNamedItem("f").getNodeValue()));
                                tempEntity.setDomesticEmployment(new StatecEntityCategory(cellNodeList.item(7).getAttributes().getNamedItem("f").getNodeValue()));
                                tempEntity.setNumberOfUnemployed(new StatecEntityCategory(cellNodeList.item(8).getAttributes().getNamedItem("f").getNodeValue()));
                                tempEntity.setActivePopulation(new StatecEntityCategory(cellNodeList.item(9).getAttributes().getNamedItem("f").getNodeValue()));

                                calculateEvolution(tempEntity.getResidentBorderers(), statecEntity.getResidentBorderers());
                                calculateEvolution(tempEntity.getNotResidentBorderers(), statecEntity.getNotResidentBorderers());
                                calculateEvolution(tempEntity.getNationalWageEarners(), statecEntity.getNationalWageEarners());
                                calculateEvolution(tempEntity.getDomesticWageEarners(), statecEntity.getDomesticWageEarners());
                                calculateEvolution(tempEntity.getNationalSelfEmployment(), statecEntity.getNationalSelfEmployment());
                                calculateEvolution(tempEntity.getDomesticSelfEmployment(), statecEntity.getDomesticSelfEmployment());
                                calculateEvolution(tempEntity.getNationalEmployment(), statecEntity.getNationalEmployment());
                                calculateEvolution(tempEntity.getDomesticEmployment(), statecEntity.getDomesticEmployment());
                                calculateEvolution(tempEntity.getNumberOfUnemployed(), statecEntity.getNumberOfUnemployed());
                                calculateEvolution(tempEntity.getActivePopulation(), statecEntity.getActivePopulation());

                                // Adding into the list of statecEntity
                                listOfEntity.add(statecEntity);

                                // Return the result (when statecEntity was not the 1st Data from XML)
                                return listOfEntity;
                            }

                            // Storing C value into entity
                            statecEntity.setResidentBorderers(new StatecEntityCategory(cellNodeList.item(0).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setNotResidentBorderers(new StatecEntityCategory(cellNodeList.item(1).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setNationalWageEarners(new StatecEntityCategory(cellNodeList.item(2).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setDomesticWageEarners(new StatecEntityCategory(cellNodeList.item(3).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setNationalSelfEmployment(new StatecEntityCategory(cellNodeList.item(4).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setDomesticSelfEmployment(new StatecEntityCategory(cellNodeList.item(5).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setNationalEmployment(new StatecEntityCategory(cellNodeList.item(6).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setDomesticEmployment(new StatecEntityCategory(cellNodeList.item(7).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setNumberOfUnemployed(new StatecEntityCategory(cellNodeList.item(8).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setActivePopulation(new StatecEntityCategory(cellNodeList.item(9).getAttributes().getNamedItem("f").getNodeValue()));

                            // If this is the oldest month (1st January 1995)
                            if (isLastDataFromXml) {
                                statecEntity.getResidentBorderers().setEvolution("0");
                                statecEntity.getNotResidentBorderers().setEvolution("0");
                                statecEntity.getNationalWageEarners().setEvolution("0");
                                statecEntity.getDomesticWageEarners().setEvolution("0");
                                statecEntity.getNationalSelfEmployment().setEvolution("0");
                                statecEntity.getDomesticSelfEmployment().setEvolution("0");
                                statecEntity.getNationalEmployment().setEvolution("0");
                                statecEntity.getDomesticEmployment().setEvolution("0");
                                statecEntity.getNumberOfUnemployed().setEvolution("0");
                                statecEntity.getActivePopulation().setEvolution("0");

                                // Adding into the list of statecEntity
                                listOfEntity.add(statecEntity);

                                // Return already the result
                                return listOfEntity;
                            } else {
                                // If isFirstDataFromXml = false, it means we still have to calculate the evolution BY COMPARING with the next XML data (the previous month).
                                needNextXmlData = true;
                            }
                        }
                    }
                }
            }
        }

        throw new NoDataFoundException("No data found for this month : " + month);
    }


    // ===========================================================================================================

    public List<StatecEntity> getSpecificRangeFromXml(String start, String end) {
        // Initialize
        List<StatecEntity> listOfEntity = new ArrayList<>();
        List<String> notFoundDateList = new ArrayList<>();
        boolean isLastDataFromXml = false;
        boolean dateFound = false;

        // Decrement start Date of 1 month to calculate its evolution
        SimpleDateFormat formatterYearMonth = new SimpleDateFormat(YEAR_MONTH_PATTERN);
        Date result;
        String newStart;
        try {
            Date startDate = formatterYearMonth.parse(start);
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.add(Calendar.MONTH, -1);
            result = cal.getTime();
            newStart = formatterYearMonth.format(result);
        } catch (ParseException e) {
            System.out.println("STATEC XML error when converting start Date for Period range: " + e);
            throw new ServerException("There is an issue when converting start Date for Period range to get another previous Month. Please call the Center.");
        }

        Document doc;
        try {
            doc = parseXML(Thread.currentThread().getContextClassLoader().getResourceAsStream("statec.xml"));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.out.println("STATEC XML file not found or can't be read : " + e);
            throw new ServerException("There is a server error. The STATEC XML file can't be found or there is a XML syntax error. Please call the Center.");
        }

        // Get the list of "Row" where they stored the Date and the value
        NodeList rowNodeList = doc.getElementsByTagName("Row");

        for (int k = 0; k < rowNodeList.getLength(); k++) {
            // If(k ==0 ), we are treating the first Data XML which is the most current Date (not the oldest)
            if (k == rowNodeList.getLength() - 1)
                isLastDataFromXml = true;

            // Check its children ("RowLabels" and "Cells")
            Node nNode = rowNodeList.item(k);

            // New instance of StatecEntity
            StatecEntity statecEntity = new StatecEntity();

            for (int j = 0; j < nNode.getChildNodes().getLength(); j++) {
                // If getNodeName() = "#text" (indent), we ignore
                if (!nNode.getChildNodes().item(j).getNodeName().equals("#text")) {

                    if (nNode.getChildNodes().item(j).getNodeName().equals("RowLabels")) {
                        // Date
                        Element eElement = (Element) nNode;
                        Element contentElement = (Element) eElement.getElementsByTagName("RowLabel").item(0);

                        for (String date : getPeriodRange(newStart, end)) {
                            if (date.equals(contentElement.getTextContent())) {
                                // Storing Date into entity
                                statecEntity.setDateValue(contentElement.getTextContent());
                                dateFound = true;

                                /*
                                 * When reading XML from most recent Date April 2021 to January 1995, keep in memory the list of date not found in XML.
                                 * For example, doing a research between March 2020 and May 2020, will add at first all of them into notFoundDateList (see below).
                                 *
                                 * As we check months in a descending order, once one of them match the data, it is removed from the list.
                                 */
                                notFoundDateList.remove(date);

                                break;
                            } else {
                                // When reading XML from most recent Date to January 1995, keep in memory the list of date not found in XML
                                notFoundDateList.add(date);
                            }
                        }
                    }

                    if (dateFound) {
                        // Then get the other values
                        if (nNode.getChildNodes().item(j).getNodeName().equals("Cells")) {
                            // Cell List
                            Element eElement2 = (Element) nNode;
                            NodeList cellNodeList = eElement2.getElementsByTagName("C");

                            // Storing C value into entity
                            statecEntity.setResidentBorderers(new StatecEntityCategory(cellNodeList.item(0).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setNotResidentBorderers(new StatecEntityCategory(cellNodeList.item(1).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setNationalWageEarners(new StatecEntityCategory(cellNodeList.item(2).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setDomesticWageEarners(new StatecEntityCategory(cellNodeList.item(3).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setNationalSelfEmployment(new StatecEntityCategory(cellNodeList.item(4).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setDomesticSelfEmployment(new StatecEntityCategory(cellNodeList.item(5).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setNationalEmployment(new StatecEntityCategory(cellNodeList.item(6).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setDomesticEmployment(new StatecEntityCategory(cellNodeList.item(7).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setNumberOfUnemployed(new StatecEntityCategory(cellNodeList.item(8).getAttributes().getNamedItem("f").getNodeValue()));
                            statecEntity.setActivePopulation(new StatecEntityCategory(cellNodeList.item(9).getAttributes().getNamedItem("f").getNodeValue()));

                            // If this is the oldest month
                            if (isLastDataFromXml) {
                                statecEntity.getResidentBorderers().setEvolution("0");
                                statecEntity.getNotResidentBorderers().setEvolution("0");
                                statecEntity.getNationalWageEarners().setEvolution("0");
                                statecEntity.getDomesticWageEarners().setEvolution("0");
                                statecEntity.getNationalSelfEmployment().setEvolution("0");
                                statecEntity.getDomesticSelfEmployment().setEvolution("0");
                                statecEntity.getNationalEmployment().setEvolution("0");
                                statecEntity.getDomesticEmployment().setEvolution("0");
                                statecEntity.getNumberOfUnemployed().setEvolution("0");
                                statecEntity.getActivePopulation().setEvolution("0");
                            }

                            // Adding into the list of statecEntity
                            listOfEntity.add(statecEntity);

                            // Reinitialize boolean
                            dateFound = false;
                        }
                    }
                }
            }
        }


        // Reverse ArrayList as first data is the most recent date
        Collections.reverse(listOfEntity);

        for (int i = 0; i < listOfEntity.size(); i++) {

            if (i > 0) {
                // Calculate the difference for each category
                calculateEvolution(listOfEntity.get(i - 1).getResidentBorderers(), listOfEntity.get(i).getResidentBorderers());
                calculateEvolution(listOfEntity.get(i - 1).getNotResidentBorderers(), listOfEntity.get(i).getNotResidentBorderers());
                calculateEvolution(listOfEntity.get(i - 1).getNationalWageEarners(), listOfEntity.get(i).getNationalWageEarners());
                calculateEvolution(listOfEntity.get(i - 1).getDomesticWageEarners(), listOfEntity.get(i).getDomesticWageEarners());
                calculateEvolution(listOfEntity.get(i - 1).getNationalSelfEmployment(), listOfEntity.get(i).getNationalSelfEmployment());
                calculateEvolution(listOfEntity.get(i - 1).getDomesticSelfEmployment(), listOfEntity.get(i).getDomesticSelfEmployment());
                calculateEvolution(listOfEntity.get(i - 1).getNationalEmployment(), listOfEntity.get(i).getNationalEmployment());
                calculateEvolution(listOfEntity.get(i - 1).getDomesticEmployment(), listOfEntity.get(i).getDomesticEmployment());
                calculateEvolution(listOfEntity.get(i - 1).getNumberOfUnemployed(), listOfEntity.get(i).getNumberOfUnemployed());
                calculateEvolution(listOfEntity.get(i - 1).getActivePopulation(), listOfEntity.get(i).getActivePopulation());
            }

        }

        if (listOfEntity.size() == 0)
            throw new NoDataFoundException("No data found for this period range : " + start + " - " + end);
        else {
            // Check if previous month of 1st item really existed

            /*
             * When doing a research for 199501-199505, we will include 199412 which doesn't exist in our XML :
             *
             * - When calling 'getPeriodRange()' it will return December 1994 -> May 1995
             * - At the end of all research, notFoundDateList should only contain December 1994
             *
             * As listOfEntity.get(0).getDateValue() = January 1995 and notFoundDateList IS NOT EMPTY,
             * if Calculated previous month is in notFoundDateList, it means IN FACT it didn't exist.
             *
             * So we don't have to remove the 1st item of List<StatecEntity>
             */
            if (isPreviousMonthReallyExisted(listOfEntity.get(0).getDateValue(), notFoundDateList))
                // Remove first item of listOfEntity. Because for range 202004-202008, we included 202003 to calculate the evolution between  202004-202003
                listOfEntity.remove(0);
            return listOfEntity;
        }
    }


    // ===========================================================================================================

    private Document parseXML(InputStream is) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(is);
        doc.getDocumentElement().normalize();
        return doc;
    }

    private List<String> getPeriodRange(String start, String end) {
        List<String> dateList = new ArrayList<>();
        DateFormat formatter = new SimpleDateFormat(YEAR_MONTH_PATTERN);
        Calendar beginCalendar = Calendar.getInstance();
        Calendar finishCalendar = Calendar.getInstance();

        try {
            beginCalendar.setTime(formatter.parse(start));
            finishCalendar.setTime(formatter.parse(end));
        } catch (ParseException e) {
            System.out.println("STATEC XML error when converting Period range: " + e);
            throw new ServerException("There is an issue when getting a list of months included in the Range. Please call the Center.");
        }
        DateFormat formatterDate = new SimpleDateFormat(MONTH_NAME_YEAR_PATTERN);

        while (beginCalendar.before(finishCalendar)) {
            // Add one month to date per loop
            String date = formatterDate.format(beginCalendar.getTime());
            dateList.add(date);
            beginCalendar.add(Calendar.MONTH, 1);
        }

        // Add last date
        String lastDate = formatterDate.format(finishCalendar.getTime());
        dateList.add(lastDate);

        return dateList;
    }

    private void calculateEvolution(StatecEntityCategory oldEntity, StatecEntityCategory currentEntity) {

        // Calculate the difference for each category
        String oldValue = oldEntity.getValue().replaceAll("\\s+", "");
        String currentValue = currentEntity.getValue().replaceAll("\\s+", "");

        int diff = Integer.parseInt(currentValue) - Integer.parseInt(oldValue);

        if (diff > 0)
            currentEntity.setEvolution("+" + diff);
        else
            currentEntity.setEvolution(String.valueOf(diff));
    }

    private boolean isPreviousMonthReallyExisted(String firstMonth, List<String> notFoundDateList) {
        // Decrement start Date of 1 month to calculate its evolution
        SimpleDateFormat formatterYearMonth = new SimpleDateFormat(MONTH_NAME_YEAR_PATTERN);
        Date result;
        String previousMonth;

        try {
            Date startDate = formatterYearMonth.parse(firstMonth);
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            cal.add(Calendar.MONTH, -1);
            result = cal.getTime();
            previousMonth = formatterYearMonth.format(result);
        } catch (ParseException e) {
            System.out.println("STATEC XML error when converting start Date for Period range: " + e);
            throw new ServerException("There is an issue when converting Date to get a previous Month for verifying its existence in notFoundDateList. Please call the Center.");
        }

        return (!notFoundDateList.contains(previousMonth));
    }
}
