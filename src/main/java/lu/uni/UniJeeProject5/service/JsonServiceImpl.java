package lu.uni.UniJeeProject5.service;

import lu.uni.UniJeeProject5.dto.StatecDto;
import lu.uni.UniJeeProject5.model.StatecEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("JsonParser")
public class JsonServiceImpl implements JsonService {

    @Autowired
    @Qualifier("JsonDto")
    private final StatecDto dto;

    public JsonServiceImpl(StatecDto dto) {
        this.dto = dto;
    }

    @Override
    public List<StatecEntity> displaySpecificMonth(String month) {
        return dto.getSpecificMonthFromXml(month);
    }

    @Override
    public List<StatecEntity> displaySpecificPeriod(String start, String end) {
        return dto.getSpecificRangeFromXml(start, end);
    }
}
