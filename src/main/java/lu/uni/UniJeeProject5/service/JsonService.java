package lu.uni.UniJeeProject5.service;

import lu.uni.UniJeeProject5.model.StatecEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface JsonService {
    List<StatecEntity> displaySpecificMonth(String month);

    List<StatecEntity> displaySpecificPeriod(String start, String end);
}
