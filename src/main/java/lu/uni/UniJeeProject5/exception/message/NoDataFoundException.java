package lu.uni.UniJeeProject5.exception.message;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoDataFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NoDataFoundException(String msg) {
        super(msg);
    }
}
