package lu.uni.UniJeeProject5.exception.message;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class ServerException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ServerException(String msg) {
        super(msg);
    }
}
