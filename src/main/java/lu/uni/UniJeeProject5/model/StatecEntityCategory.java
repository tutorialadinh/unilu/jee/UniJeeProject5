package lu.uni.UniJeeProject5.model;

import lombok.Getter;
import lombok.Setter;

public class StatecEntityCategory {

    @Getter
    @Setter
    private String value;

    @Getter
    @Setter
    private String evolution;

    // Constructor
    public StatecEntityCategory(String value) {
        this.value = value;
    }
}
