package lu.uni.UniJeeProject5.model;

import lombok.Getter;
import lombok.Setter;

public class StatecEntity {

    @Getter
    @Setter
    private String dateValue;

    @Getter
    @Setter
    private StatecEntityCategory residentBorderers;

    @Getter
    @Setter
    private StatecEntityCategory notResidentBorderers;

    @Getter
    @Setter
    private StatecEntityCategory nationalWageEarners;

    @Getter
    @Setter
    private StatecEntityCategory domesticWageEarners;

    @Getter
    @Setter
    private StatecEntityCategory nationalSelfEmployment;

    @Getter
    @Setter
    private StatecEntityCategory domesticSelfEmployment;

    @Getter
    @Setter
    private StatecEntityCategory nationalEmployment;

    @Getter
    @Setter
    private StatecEntityCategory domesticEmployment;

    @Getter
    @Setter
    private StatecEntityCategory numberOfUnemployed;

    @Getter
    @Setter
    private StatecEntityCategory activePopulation;
}
